const express = require('express');
const mongoose = require('mongoose');
const multer = require('multer');
const checkAuth = require('../middleware/check-auth');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Seller = require('../models/seller');
const Errors = require('../../constants/errors');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads/');
    },
    filename: (req, file, cb) => {
        cb(null, new Date().getTime()+ "." + file.originalname.split('.')[file.originalname.split('.').length-1]);
    }
});
const upload = multer({storage: storage, limits: {
        fileSize: 1024 * 1024 * 5
    }, 
    fileFilter: (req, file, cb) => {
        if(file.mimetype == 'image/jpeg' || file.mimetype == 'image/png'){
            cb(null, true);
        } else {
            cb(new Error("Uploaded file is not an image"), false)
        }
    }});

router.get('/', checkAuth, async (req, res, next) => {
    try{
        const sellers = await Seller.find();
        res.status(200).json(sellers.map(seller => {
            return {
                _id: seller.id,
                name: seller.name,
                email: seller.email,
                phone: seller.phone,
                rating: seller.rating,
                services: seller.services,
                img: seller.img,
                slots: seller.slots,
                type: seller.type,
                location: seller.location
            }
        }));
    } catch(err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
})

router.get('/:id', checkAuth, async (req, res, next) => {
    try{
        const seller = await Seller.findById(req.params.id);
        res.status(200).json({
            _id: seller.id,
            name: seller.name,
            email: seller.email,
            phone: seller.phone,
            rating: seller.rating,
            services: seller.services,
            img: seller.img,
            slots: seller.slots,
            type: seller.type,
            location: seller.location
        });
    } catch(err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
})

router.post('/login', async (req, res, next) => {
    try {
        const seller = await Seller.findOne({ email: req.body.email })
        if(!seller){
            return res.status(401).json({
                errors: {
                    message: Errors.AUTH_FAIL
                }
            });
        }    
        bcrypt.compare(req.body.password, seller.password, async (err, result) => {
            try{
                if(err){
                    return res.status(401).json({
                        errors: {
                            message: Errors.AUTH_FAIL
                        }
                    });
                }
                if(result){
                    const token = jwt.sign({ email: seller.email, userId: seller._id }, process.env.JWT_KEY, {
                        expiresIn: "1h"
                    });
                    return res.status(200).json({
                        token: token,
                        user: {
                            _id: seller.id,
                            name: seller.name,
                            email: seller.email,
                            phone: seller.phone,
                            rating: seller.rating,
                            services: seller.services,
                            img: seller.img,
                            slots: seller.slots,
                            type: seller.type,
                            location: seller.location
                        }
                    });
                } else {
                    res.status(401).json({
                        errors: {
                            message: Errors.AUTH_FAIL
                        }
                    });
                }
            } catch(err) {
                res.status(404).json({
                    errors: {
                        message: err.message
                    }
                });
            }
        })
        
    } catch (err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
})

router.get('/search/:query', checkAuth, async (req, res, next) => {
    try{
        const query = req.params.query;
        const sellers = await Seller.find({$or:[
            {name:{"$regex": query, '$options': 'i'}},
            {services:{"$regex": query, '$options': 'i'}}
        ]});
        res.status(200).json(sellers);
    } catch(err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
})

router.post('/', async (req, res, next) => {
    try {
        //Check if Seller exists
        const seller = await Seller.findOne({ email: req.body.email });
        if(seller){
            res.status(400).json({
                errors: {
                    message: Errors.USER_EXISTS
                }
            });
        } else {
            if(!req.body.password){
                res.status(400).json({
                    errors: {
                        message: Errors.MISSING_PWD
                    }
                });
            }
            bcrypt.hash(req.body.password, 10, async (err, hash) => {
                try{
                    if(err){
                        res.status(500).json({
                            errors: {
                                message: err.message
                            }
                        });
                    } else {
                        const seller = new Seller({
                            _id: mongoose.Types.ObjectId(),
                            name: req.body.name,
                            email: req.body.email,
                            phone: req.body.phone,
                            password: hash
                        });
                        await seller.save();
                        const token = jwt.sign({ email: seller.email, sellerId: seller._id }, process.env.JWT_KEY, {
                            expiresIn: "1h"
                        });
                        res.status(200).json({
                            user: {
                                _id: seller.id,
                                name: seller.name,
                                email: seller.email,
                                phone: seller.phone,
                                rating: seller.rating,
                                services: seller.services,
                                img: seller.img,
                                slots: seller.slots,
                                type: seller.type,
                                location: seller.location
                            },
                            token: token
                        });
                    }
                } catch (err) {
                    res.status(400).json({
                        errors: {
                            message: err.message
                        }
                    });
                }
            });
        }
    } catch(err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
})

router.patch('/:id', checkAuth, upload.single('img'), async (req, res, next) => {
    try {
        reqArr = Object.entries(req.body);
        const updateOps = {};
        for(const ops of reqArr){
            console.log(ops[0], ops[1])
            if(ops[0] == 'services'){
                updateOps[ops[0]] = ops[1].split(',');
            } else {
                updateOps[ops[0]] = ops[1];
            }
        }
        if(req.file){
            updateOps.img = req.file.path
        }
        const seller = await Seller.findByIdAndUpdate(req.params.id, { $set: updateOps }, { new: true, lean: true })
        res.status(200).json(seller);
    } catch (err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
})

module.exports = router;