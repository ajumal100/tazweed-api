const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const checkAuth = require('../middleware/check-auth');

const User = require('../models/user');
const Errors = require('../../constants/errors');

const router = express.Router();

mongoose.set('useCreateIndex', true);

router.post('/login', async (req, res, next) => {
    try {
        const user = await User.findOne({ email: req.body.email })
        if(!user){
            return res.status(401).json({
                errors: {
                    message: Errors.USER_NOT_FOUND
                }
            });
        }    
        bcrypt.compare(req.body.password, user.password, async (err, result) => {
            try{
                if(err){
                    return res.status(401).json({
                        errors: {
                            message: Errors.AUTH_FAIL
                        }
                    });
                }
                if(result){
                    const token = jwt.sign({ email: user.email, userId: user._id }, process.env.JWT_KEY, {
                        expiresIn: "1h"
                    });
                    return res.status(200).json({
                        token: token,
                        user: {
                            _id: user._id,
                            name: user.name,
                            email: user.email,
                            phone: user.phone
                        }
                    });
                } else {
                    res.status(401).json({
                        errors: {
                            message: Errors.AUTH_FAIL
                        }
                    });
                }
            } catch(err) {
                res.status(404).json({
                    errors: {
                        message: err.message
                    }
                });
            }
        })
        
    } catch (err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
})

router.post('/', async (req, res, next) => {
    //Check if User exists
    try {
        const user = await User.findOne({ email: req.body.email })
        if(user){
            res.status(400).json({
                errors: {
                    message: Errors.USER_EXISTS
                }
            });
        } else {
            //Save User
            if(!req.body.password){
                res.status(400).json({
                    errors: {
                        message: Errors.MISSING_PWD
                    }
                });
            }
            bcrypt.hash(req.body.password, 10, async (err, hash) => {
                try{
                    if(err){
                        res.status(500).json({
                            errors: {
                                message: err.message
                            }
                        });
                    } else {
                        const user = new User({
                            _id: mongoose.Types.ObjectId(),
                            name: req.body.name,
                            email: req.body.email,
                            phone: req.body.phone,
                            password: hash
                        });
                        await user.save();
                        const token = jwt.sign({ email: user.email, userId: user._id }, process.env.JWT_KEY, {
                            expiresIn: "1h"
                        });
                        res.status(200).json({
                            user: {
                                _id: user._id,
                                name: user.name,
                                email: user.email,
                                phone: user.phone
                            },
                            token: token
                        });
                    }
                } catch (err) {
                    res.status(400).json({
                        errors: {
                            message: err.message
                        }
                    });
                }
            });
        }
    } catch (err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
})

router.patch('/:id', checkAuth, async (req, res, next) => {
    try {
        reqArr = Object.entries(req.body);
        const updateOps = {};
        for(const ops of reqArr){
            if(['name', 'phone'].includes(ops[0])){
                updateOps[ops[0]] = ops[1];
            }
        }
        const seller = await User.updateOne({_id: req.params.id}, { $set: updateOps });
        res.status(200).json(seller);
    }
    catch (err) {
        res.status(400).json({
            errors: {
                messgae: err.message
            }
        });
    }
})

module.exports = router;