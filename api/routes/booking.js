const express = require('express');
const mongoose = require('mongoose');

const Booking = require('../models/booking');
const checkAuth = require('../middleware/check-auth');
const Errors = require('../../constants/errors');

const router = express.Router();

router.get('/user/:id', checkAuth, async (req, res, next) => {
    try {
        const bookings = await Booking.find({ user: req.params.id }).populate('seller');
        res.status(200).json(bookings.map(booking => {
            return {
                _id: booking.id,
                seller: booking.seller,
                user: booking.user,
                date: booking.date,
                time: booking.time,
                status: booking.status,
                createdAt: booking.createdAt,
                seller: {
                    _id: booking.seller._id,
                    name: booking.seller.name,
                    email: booking.seller.email,
                    phone: booking.seller.phone,
                    img: booking.seller.img,
                    rating: booking.seller.rating,
                    services: booking.seller.services,
                    type: booking.seller.type,
                    location: booking.seller.location
                }
            }
        }));
    } catch(err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
});

router.get('/seller/:id', checkAuth, async (req, res, next) => {
    try {
        const bookings = await Booking.find({ seller: req.params.id }).populate('user');
        res.status(200).json(bookings.map(booking => {
            return {
                _id: booking.id,
                seller: booking.seller,
                user: booking.user,
                date: booking.date,
                time: booking.time,
                status: booking.status,
                createdAt: booking.createdAt,
                user: {
                    _id: booking.user._id,
                    name: booking.user.name,
                    email: booking.user.email,
                    phone: booking.user.phone
                }
            }
        }));
    } catch(err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
});

router.get('/:id', checkAuth, async (req, res, next) => {
    try {
        const booking = await Booking.findById(req.params.id);
        res.status(200).json(booking);
    } catch(err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
});

router.post('/', checkAuth, async (req, res, next) => {
    try {
        const booking = new Booking({
            _id: new mongoose.Types.ObjectId(),
            user: req.body.user,
            seller: req.body.seller,
            date: req.body.date,
            time: req.body.time
        });
        const response = await booking.save();
        res.status(200).json(response);
    } catch(err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
});

router.patch('/:id', checkAuth, async (req, res, next) => {
    try {
        reqArr = Object.entries(req.body);
        const updateOps = {};
        for(const ops of reqArr){
            updateOps[ops[0]] = ops[1];
        }
        console.log(updateOps);
        const seller = await Booking.findByIdAndUpdate(req.params.id, { $set: updateOps }, { new: true, lean: true });
        res.status(200).json(seller);
    }
    catch (err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
});

router.delete('/:id', checkAuth, async (req, res, next) => {
    try {
        const response = await Booking.findOneAndDelete({'_id' : req.params.id});
        res.status(200).json(response);
    } catch(err) {
        res.status(400).json({
            errors: {
                message: err.message
            }
        });
    }
})


module.exports = router;