const jwt = require('jsonwebtoken');
const Errors = require('../../constants/errors');

module.exports = (req, res, next) => {
    try {
        req.userData = jwt.verify(req.headers.authorization.split(' ')[1], process.env.JWT_KEY);
        next();
    } catch(err) {
        return res.status(401).json({
            errors: {
                message: err.message
            }
        })
    }
}