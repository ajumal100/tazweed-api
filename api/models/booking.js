const mongoose = require('mongoose');

const bookingSchema = mongoose.Schema({
    _id: mongoose.SchemaTypes.ObjectId,
    seller: { type: mongoose.SchemaTypes.ObjectId, ref: 'Seller' },
    user: { type: mongoose.SchemaTypes.ObjectId, ref: 'User' },
    date: { type: String, required: true },
    time: { type: String, required: true },
    status: { type: String, default: "pending" },
    createdAt: { type: Date, default: Date.now }
})

module.exports = mongoose.model("Booking", bookingSchema);