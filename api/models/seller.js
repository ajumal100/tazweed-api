const mongoose = require('mongoose');

const sellerSchema = mongoose.Schema({
    _id: mongoose.SchemaTypes.ObjectId,
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true, match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ },
    phone: { type: String, default: "" },
    password: { type: String, required: true },
    img: { type: String, default: "" },
    rating: { type: String, default: "4.6" },
    services: [{ type: String }],
    createdAt: { type: Date, default: Date.now },
    slots: { type: String, default: ""},
    type: { type: String, default: "" },
    location: { type: String, default: ""}
})

module.exports = mongoose.model("Seller", sellerSchema);