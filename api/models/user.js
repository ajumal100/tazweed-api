const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.SchemaTypes.ObjectId,
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true, match: /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/ },
    phone: { type: String, default: '' },
    password: { type: String, required: true },
    createdAt: { type: Date, default: Date.now }
})

module.exports = mongoose.model("User", userSchema);