const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const userRouter = require('./api/routes/user');
const sellerRouter = require('./api/routes/seller');
const bookingRouter = require('./api/routes/booking');

mongoose.connect("mongodb+srv://root:"+ process.env.MONGO_PW +"@tazweed0-t62wt.mongodb.net/test?retryWrites=true&w=majority",
    { 
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: true
    });

app.use('/uploads',express.static('uploads'));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, GET, DELETE');
        return res.status(200).json({});
    }
    next();
})

app.use('/users', userRouter);
app.use('/sellers', sellerRouter);
app.use('/bookings', bookingRouter);

app.use((req, res, next) => {
    const error = new Error("Not FOund");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});

module.exports = app;